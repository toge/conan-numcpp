#include <iostream>
#include "NumCpp.hpp"

int main() {
    auto a1 = nc::NdArray<int>{ {1, 2}, {3, 4}, {5, 6} };
    a1.reshape(2, 3);
}
