import os

from conans import ConanFile, tools


class NumcppConan(ConanFile):
    name = "numcpp"
    version = "2.3.0"
    license = "MIT"
    author = "toge.mail@gmail.com"
    url = "https://bitbucket.org/toge/conan-numcpp/"
    homepage = "https://github.com/dpilger26/NumCpp"
    description = "C++ implementation of the Python Numpy library"
    topics = ("mathmatical-functions", "numerical-analysis", "numpy")
    no_copy_source = True
    requires = ("boost/[>=1.68]")
    # No settings/options are necessary, this is header only

    def source(self):
        '''retrieval of the source code here. Remember you can also put the code
        in the folder and use exports instead of retrieving it with this
        source() method
        '''
        tools.get("https://github.com/dpilger26/NumCpp/archive/Version_{}.zip".format(self.version))
        os.rename("NumCpp-Version_{}".format(self.version), "numcpp")

        # self.run("git clone ...") or
        # tools.download("url", "file.zip")
        # tools.unzip("file.zip" )

    def package(self):
        self.copy("*.hpp", "include", src="numcpp/include")
